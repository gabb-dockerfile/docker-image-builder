FROM gcr.io/kaniko-project/executor:debug as kaniko

FROM debian:buster-slim

COPY --from=kaniko /kaniko/executor /kaniko/
COPY --from=kaniko /kaniko/docker-credential-ecr-login /kaniko/
COPY --from=kaniko /kaniko/.docker/config.json /kaniko/.docker/

ENV PATH=/kaniko:$PATH
ENV DOCKER_CONFIG /kaniko/.docker/

RUN apt-get update && \
    apt install ca-certificates make -y && \
    rm -rf /var/lib/apt/lists/*

COPY ./scripts /opt/scripts
RUN chmod -R +x /opt/scripts && \
    ln -s /opt/scripts/* /usr/bin && \
    mkdir -p /kaniko/.docker

ENTRYPOINT [""]
